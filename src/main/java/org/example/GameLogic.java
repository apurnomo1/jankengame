package org.example;

import java.util.HashMap;
import java.util.List;

public class GameLogic {
    Rock rock;
    Paper paper;
    Scissor scissor;

    private HashMap<String, Hand> hands;

    GameLogic() {
        this.paper = new Paper();
        this.rock = new Rock();
        this.scissor = new Scissor();
        this.hands = new HashMap<>();

        this.addLogic(this.paper, this.rock);
        this.addLogic(this.rock, this.scissor);
        this.addLogic(this.scissor, this.paper);

        hands.put(this.paper.getKey(), this.paper);
        hands.put(this.scissor.getKey(), this.scissor);
        hands.put(this.rock.getKey(), this.rock);

    }

    private void addLogic(Hand win, Hand lose) {
        win.addWinTo(lose);
        lose.addLoseTo(win);
    }

    public HashMap<String, Hand> getHands() {
        return this.hands;
    }
}
