package org.example;

import java.util.ArrayList;
import java.util.List;

public class Hand {

    public static enum WinStatus {
        WIN,
        LOSE,
        DRAW
    }

    private List<Hand> loseTo;
    private List<Hand> winTo;
    
    private String key;
    
    private String displayName;
    Hand() {
        this.loseTo = new ArrayList<Hand>();
        this.winTo = new ArrayList<Hand>();
    }

    public WinStatus playAgainst(Hand otherHand){
        if (loseTo.contains(otherHand)) return WinStatus.LOSE;
        else if (winTo.contains(otherHand)) return WinStatus.WIN;
        else return WinStatus.DRAW;
    }

    public void addLoseTo(Hand otherHand) {
        loseTo.add(otherHand);
    }

    public void clearLoseTo() {
        loseTo.clear();
    }

    public void addWinTo(Hand otherHand) {
        winTo.add(otherHand);
    }

    public void clearWinTo() {
        winTo.clear();
    }
    
    public void setNames(String key,String displayName){
        this.key = key;
        this.displayName = displayName;
    }

    public String getKey() {
        return this.key;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
