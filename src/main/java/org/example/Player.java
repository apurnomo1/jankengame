package org.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Player {
    private int id;
    private String name;

    private Hand hand;
    private Boolean isBot;

    Player(String name, int id, Boolean isBot) {
        this.name = name;
        this.id = id;
        this.isBot = isBot;
    }

    public boolean chooseHand(String key, HashMap<String, Hand> hands) {
        if (hands.containsKey(key)) {
            this.hand = hands.get(key);
            return true;
        } else {
            return false;
        }
    }

    public void randomizedHand(HashMap<String, Hand> hands) {
        List<Hand> valuesList = new ArrayList<Hand>(hands.values());
        int randomIndex = new Random().nextInt(valuesList.size());
        this.hand = valuesList.get(randomIndex);
    }

    public int getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }

    public Hand getHand() {
        return this.hand;
    }

    public Boolean getBot() {
        return isBot;
    }
}
