package org.example;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;


public class GameController {
    private static final Integer ERROR_THRESHOLD = 3;
    private static final Integer DRAW_THRESHOLD = 3;
    private GameLogic gameLogic;
    private List<Player> players;
    private Scanner scanner;


    private Integer errorCount;
    private Integer drawCount;

    GameController(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
        this.scanner = new Scanner(System.in);
        this.errorCount = 0;
        this.drawCount = 0;
        this.players = new ArrayList<>();
    }

    public void play() {
        System.out.println("Welcome to the Janken Game!");

        int numPlayer = 0;

        while (errorCount < ERROR_THRESHOLD) {
            System.out.println("Please enter the number of player (1 or 2):");
            numPlayer = scanner.nextInt();


            if (numPlayer == 1 || numPlayer == 2) {
                this.errorCount = 0;
                break;
            } else {
                this.errorCount += 1;
            }
        }

        for (int i = 1; i <= numPlayer; ++i) {
            this.players.add(new Player("Player" + Integer.toString(i), i, false));
        }
        if (numPlayer == 1) {
            this.players.add(new Player("Player" + Integer.toString(numPlayer + 1) + " (Bot)", numPlayer + 1, true));
        }


        while (drawCount < DRAW_THRESHOLD) {
            if (this.match()) {
                drawCount = 0;
                break;
            } else {
                drawCount += 1;
            }
        }
    }

    private boolean match() {

        HashMap<String, Hand> hands = this.gameLogic.getHands();
        for (Player player : this.players) {
            if (player.getBot()) {
                player.randomizedHand(hands);
                continue;
            }

            while (errorCount < ERROR_THRESHOLD) {
                System.out.println(player.getName() + ", please choose your hand from the list below ");
                hands.forEach((key, hand) -> {
                    System.out.println("Input " + key + " for " + hand.getDisplayName());
                });
                String key = scanner.next();
                if (player.chooseHand(key, hands)) {
                    errorCount = 0;
                    clearScreen();
                    break;
                } else {
                    errorCount += 1;
                    System.out.println("Invalid input");
                }
                clearScreen();
            }
        }
        displayPlayersChoice();
        return decideWinner(players);
    }

    public void displayPlayersChoice() {
        for(Player player : this.players) {
            System.out.println(player.getName() + "chose " + player.getHand().getDisplayName());
        }
    }

    public boolean decideWinner(List<Player> players) {

        Player p1 = players.get(0);
        Player p2 = players.get(1);

        Hand p1Hand = p1.getHand();
        Hand p2Hand = p2.getHand();

        if(p1Hand.playAgainst(p2Hand).equals(Hand.WinStatus.WIN)) {
            System.out.println(p1.getName() + " wins the game.");
        } else if (p1Hand.playAgainst(p2Hand).equals(Hand.WinStatus.LOSE)) {
            System.out.println(p2.getName() + " wins the game.");
        }
        else {
            System.out.println("Draw, replay the game");
            return false;
        }
        return true;
    }

    private void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}